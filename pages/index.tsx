import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>usatime.info</title>
        <meta name="description" content="News" />
        <link rel="icon" href="/vercel.svg" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to usatime.info
        </h1>

        <div className={styles.grid}>
          <a href="https://usatime.info/" className={styles.card}>
            <h2>Mammoth 80kg fish reeled in by stunned fisherman using a line designed to handle a 14kg catch</h2>
            <img src="/anh1.jfif" />
            <p>A fisherman has shared pictures of the massive once-in-a-lifetime 80kg fish he reeled in while angling off the south Queensland coast.</p>
          </a>

          <a href="https://usatime.info/" className={styles.card}>
            <h2>‘Amazing’ halibut, one of the largest fish in the Gulf of Maine, are making a comeback</h2>
            <img src="/anh2.webp" />
            <p>Halibυt are oпe of the largest fish iп the Gυlf of Maiпe, secoпd oпly to blυefiп tυпa, swordfish aпd large sharks.</p>
          </a>

          <a href="https://usatime.info/" className={styles.card}>
            <h2>Now THAT’S a catch! Boy, 12, hooks 11-foot, 500lb great white shark on charter fishing trip in Florida</h2>
            <img src="/anh3.avif" />
            <p>A 12-year-old boy scored the catch of a lifetime after he hooked a massive 11-foot, 500-pound great white shark while on a chartered fishing trip.</p>
          </a>

          <a href="https://usatime.info/" className={styles.card}>
            <h2>Japanese fisherman reels in massive fish caught off the coast of Japan</h2>
            <img src="/anh4.jpg" />
            <p>Visibly straining as he holds it aloft, a Japanese fisherman grimaces as he proudly displays a terrifyingly large fish caught in the waters off Japan.</p>
          </a>

          <a href="https://usatime.info/" className={styles.card}>
            <h2>The cod-father: Swedish dad and his son reel in enormous 5ft-long fish off Norwegian island</h2>
            <img src="/anh5.avif" />
            <p>A Swedish father and son had the fishing trip of a lifetime after they managed to reel in an enormous 5ft-long cod which is big enough to feed 35 people.</p>
          </a>

          <a href="https://usatime.info/" className={styles.card}>
            <h2>The 10 Best Fishing Lakes in New York</h2>
            <img src="/anh6.jpg" />
            <p>A fisherman has shared pictures of the massive once-in-a-lifetime 80kg fish he reeled in while angling off the south Queensland coast.</p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}

export default Home
